package com.vimal.code.ToDo;

import com.vimal.code.ToDo.Model.Role;
import com.vimal.code.ToDo.Model.Roles_Name;
import com.vimal.code.ToDo.repo.RoleRepository;
import com.vimal.code.ToDo.repo.UserRepo;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
public class ToDoAppApplication {
	private final UserRepo userRepository;
	private final RoleRepository roleRepository;
	public ToDoAppApplication(UserRepo userRepository, RoleRepository roleRepository){
		this.userRepository = userRepository;
		this.roleRepository = roleRepository;
	}


	public static void main(String[] args) {
		SpringApplication.run(ToDoAppApplication.class, args);
	}
	@Bean
	public ModelMapper modelMapper(){
		return new ModelMapper();
	}
	@PostConstruct
	public void migrationData() {
		var checkUser = userRepository.findByName("admin");
		if (checkUser.isEmpty()) {
			var checkRoleAdmin = roleRepository.findByName(Roles_Name.ROLE_ADMIN);
			if (checkRoleAdmin.isEmpty()) {
				Role role = new Role();
				role.setName(Roles_Name.ROLE_ADMIN);
				roleRepository.save(role);
			}
			var checkRolePM = roleRepository.findByName(Roles_Name.ROLE_PM);
			if (checkRolePM.isEmpty()) {
				Role role = new Role();
				role.setName(Roles_Name.ROLE_PM);
				roleRepository.save(role);
			}
			var checkRoleUser = roleRepository.findByName(Roles_Name.ROLE_USER);
			if (checkRoleUser.isEmpty()) {
				Role role = new Role();
				role.setName(Roles_Name.ROLE_USER);
				roleRepository.save(role);
			}
		}
	}
}
