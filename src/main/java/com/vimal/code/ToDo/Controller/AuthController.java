package com.vimal.code.ToDo.Controller;

import com.vimal.code.ToDo.Auth.JwtHelper;
import com.vimal.code.ToDo.Model.Role;
import com.vimal.code.ToDo.Model.Roles_Name;
import com.vimal.code.ToDo.Model.response.MessageResponse;
import com.vimal.code.ToDo.config.AuthConfig;
import com.vimal.code.ToDo.constant.Constants;
import com.vimal.code.ToDo.payload.Request.LoginRequest;
import com.vimal.code.ToDo.payload.Request.CreateRequest;
import com.vimal.code.ToDo.payload.Response.JwtResponse;
import com.vimal.code.ToDo.payload.Response.UserResponse;
import com.vimal.code.ToDo.Expecption.UserAlreadyExistsException;
import com.vimal.code.ToDo.repo.RoleRepository;
import com.vimal.code.ToDo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController
@CrossOrigin(value = "*", maxAge = 3600)
@RequestMapping("/api/auth")
public class AuthController {
    private final RoleRepository roleRepository;
    public AuthController(RoleRepository roleRepository){
        this.roleRepository = roleRepository;
    }
    @Autowired
    private AuthConfig authConfig;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private AuthenticationManager manager;
    @Autowired
    private JwtHelper helper;


    @Autowired
    private UserService userService;
    private MessageResponse messageResponse;


    @PostMapping("/create")
    public ResponseEntity<JwtResponse> createUser(@RequestBody CreateRequest createRequest) {
        try {
            UserResponse userResponseDto = userService.createUser(createRequest);
            UserDetails userDetails = userDetailsService.loadUserByUsername(userResponseDto.getEmail());

            Set<String> strRoles = createRequest.getRole();
            Set<Role> roles = new HashSet<>();
            if (strRoles == null) {
                Role userRole = roleRepository.findByName(Roles_Name.ROLE_USER)
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
            } else {
                strRoles.forEach(role -> {
                    switch (role) {
                        case "admin" -> {
                            Role adminRole = roleRepository.findByName(Roles_Name.ROLE_ADMIN)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(adminRole);
                        }
                        case "pm" -> {
                            Role modRole = roleRepository.findByName(Roles_Name.ROLE_PM)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(modRole);
                        }
                        default -> {
                            Role userRole = roleRepository.findByName(Roles_Name.ROLE_USER)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(userRole);
                        }
                    }
                });
            }
            String token = this.helper.generateToken(userDetails);
            JwtResponse jwtResponse = JwtResponse.builder().token(token).build();
            return new ResponseEntity<>(jwtResponse, HttpStatus.CREATED);
        } catch (UserAlreadyExistsException ex) {
            // Handle the exception and return an appropriate response
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new JwtResponse("User already exists: " + ex.getMessage()));
        }
    }


    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody LoginRequest jwtRequest) {
           this.doAuthenticate(jwtRequest.getEmail(), jwtRequest.getPassword());
           UserDetails userDetails = userDetailsService.loadUserByUsername(jwtRequest.getEmail());
           String token = this.helper.generateToken(userDetails);
           JwtResponse jwtResponse = JwtResponse.builder().token(token).build();
           String generated_token = jwtResponse.getToken();
           messageResponse = new MessageResponse();
           messageResponse.loginSuccess(userDetails,generated_token);
           return new ResponseEntity<>(messageResponse, HttpStatus.OK);
    }

    private void doAuthenticate(String email, String password) {

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(email, password);
        try {
            manager.authenticate(authentication);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException(" Invalid Username or Password  !!");

        }
    }

    @ExceptionHandler(BadCredentialsException.class)
    public String exceptionHandler(BadCredentialsException ex) {
        return "Credentials Invalid !!";
    }
}
