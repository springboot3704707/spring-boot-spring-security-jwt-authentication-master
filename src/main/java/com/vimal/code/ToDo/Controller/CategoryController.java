package com.vimal.code.ToDo.Controller;

import com.vimal.code.ToDo.Model.Categories;
import com.vimal.code.ToDo.Model.request.BaseRequest;
import com.vimal.code.ToDo.Model.response.MessageResponse;
import com.vimal.code.ToDo.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/category")
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
public class CategoryController {
    private final CategoryService categoryService;
    private MessageResponse messageResponse;
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @PostMapping("/list")
    public ResponseEntity<Object> getAll(@RequestBody BaseRequest request){
        try {
            List<Categories> category = categoryService.findAllCategoryByStatus(request.getStatus());
            messageResponse = new MessageResponse();
            messageResponse.getSuccess(category);
            return new ResponseEntity<>(messageResponse, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> create(@RequestBody Categories request){
        try {
            categoryService.save(request);
            messageResponse = new MessageResponse();
            messageResponse.createSuccess("Created success");
            return new ResponseEntity<>(messageResponse, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/{id}")
    public ResponseEntity<Object> getById(@PathVariable("id") Integer id) {
       try{
           Categories category = categoryService.findById(id);
           messageResponse = new MessageResponse();
           messageResponse.getSuccess(category);
           return new ResponseEntity<>(messageResponse, HttpStatus.OK);
       }catch (Exception e){
           return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
       }
    }
    @PostMapping("/update")
    public ResponseEntity<Object> update(@RequestBody Categories request){
        try{
            categoryService.update(request);
            messageResponse = new MessageResponse();
            messageResponse.updateSuccess("Update Success");
            return new ResponseEntity<>(messageResponse,HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/delete")
    public ResponseEntity<Object> delete(@RequestBody Categories request){
        try{
            categoryService.delete(request);
            messageResponse = new MessageResponse();
            messageResponse.updateSuccess("Delete Success");
            return new ResponseEntity<>(messageResponse,HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
