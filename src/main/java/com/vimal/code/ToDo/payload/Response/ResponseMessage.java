package com.vimal.code.ToDo.payload.Response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResponseMessage {
    private String code;
    private String message;
    private String token;

    public void saveMessage(String token){
        this.code = "200";
        this.message = "Your Account Register Succeed! ";
        this.token = token;
    }
    public void errorMessage(){
        this.code = "400";
        this.message = "Something when wrong! ";
    }
}
