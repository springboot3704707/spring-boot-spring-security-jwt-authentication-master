package com.vimal.code.ToDo.payload.Response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponse {
    private long  id;
    private String name;
    private String email;
}
