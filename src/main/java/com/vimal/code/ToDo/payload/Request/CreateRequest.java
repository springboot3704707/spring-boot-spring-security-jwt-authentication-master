package com.vimal.code.ToDo.payload.Request;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class CreateRequest {
        private String name;
        private String email;
        private String password;
        private Set<String> role;
}
