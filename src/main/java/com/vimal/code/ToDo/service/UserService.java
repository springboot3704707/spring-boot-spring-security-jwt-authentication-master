package com.vimal.code.ToDo.service;

import com.vimal.code.ToDo.payload.Request.CreateRequest;
import com.vimal.code.ToDo.payload.Response.UserResponse;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    List<UserResponse> getAllUser();

    public UserResponse createUser(CreateRequest userRequestDto);

}
