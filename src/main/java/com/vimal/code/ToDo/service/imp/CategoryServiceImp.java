package com.vimal.code.ToDo.service.imp;

import com.vimal.code.ToDo.Model.Categories;
import com.vimal.code.ToDo.Model.response.AppException;
import com.vimal.code.ToDo.constant.Constants;
import com.vimal.code.ToDo.repo.CategoryRepo;
import com.vimal.code.ToDo.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class CategoryServiceImp implements CategoryService {
    private final CategoryRepo categoryRepo;

    public CategoryServiceImp(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }

    @Override
    public List<Categories> findAllCategoryByStatus(String status) throws Exception {
        if (status.equals("ALL")){
            return categoryRepo.findAllByStatusInOrderByIdDesc(Constants.getAllStatusString());
        }
        return categoryRepo.findAllByStatus(status);
    }

    @Override
    public void save(Categories request) throws Exception {
        request.setStatus(Constants.STATUS_ACTIVE);
        categoryRepo.save(request);
    }

    @Override
    public Categories findById(Integer id) throws AppException {
        return categoryRepo.findById(id).orElse(null);
    }

    @Override
    public void update(Categories request) throws Exception {
        Categories category = findById(request.getId());
        if (category !=null){
            category.setStatus(Constants.STATUS_ACTIVE);
            category.setName(request.getName());
            category.setNameKh(request.getNameKh());
            categoryRepo.save(category);
        }
    }

    @Override
    public void delete(Categories request) throws Exception {
        Categories category = findById(request.getId());
        if (category !=null){
            category.setStatus(Constants.STATUS_DELETE);
            categoryRepo.save(category);
        }
    }
}
