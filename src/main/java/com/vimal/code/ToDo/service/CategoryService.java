package com.vimal.code.ToDo.service;

import com.vimal.code.ToDo.Model.Categories;
import com.vimal.code.ToDo.Model.response.AppException;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    List<Categories> findAllCategoryByStatus(String status) throws Exception;
    void save(Categories request) throws Exception;
    Categories findById (Integer id) throws AppException;
    void update(Categories request) throws Exception;
    void delete(Categories request) throws Exception;
}
