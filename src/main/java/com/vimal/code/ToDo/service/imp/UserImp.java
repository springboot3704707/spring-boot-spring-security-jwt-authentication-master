package com.vimal.code.ToDo.service.imp;

import com.vimal.code.ToDo.config.AuthConfig;
import com.vimal.code.ToDo.payload.Request.CreateRequest;
import com.vimal.code.ToDo.payload.Response.UserResponse;
import com.vimal.code.ToDo.Model.User;
import com.vimal.code.ToDo.Expecption.UserAlreadyExistsException;
import com.vimal.code.ToDo.repo.UserRepo;
import com.vimal.code.ToDo.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserImp implements UserService {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private AuthConfig authConfig;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByEmail(username).orElseThrow(()->new RuntimeException("User not found"));
        System.out.println("Retrived Data");
        System.out.println(user.getPassword()+"Retrived Password");
        System.out.println(user.getUsername());
        System.out.println(user.getId());
        System.out.println(user.getEmail());
        System.out.println("-----");
        return user;
    }

    @Override
    public List<UserResponse> getAllUser() {
        List<User> userEnitiys = userRepo.findAll();
        List<UserResponse> userResponseDtoList = userEnitiys.stream().map(user->this.userEntityToUserRespDto(user)).collect(Collectors.toList());
        return userResponseDtoList;
    }
    @Override
    public UserResponse createUser(CreateRequest userRequestDto) {
        Optional<User> foundUser = this.userRepo.findByEmail(userRequestDto.getEmail());
        if (foundUser.isEmpty()) {
            User user = this.userReqDtoToUserEntity(userRequestDto);
            user.setPassword(authConfig.passwordEncoder().encode(user.getPassword()));
            User createdUser = userRepo.save(user);
            return this.userEntityToUserRespDto(createdUser);
        } else {
            // User already exists, throw an exception
            throw new UserAlreadyExistsException("User with email " + userRequestDto.getEmail() + " already exists");
        }
    }



    public User userReqDtoToUserEntity(CreateRequest userReqDto){
        User user = this.modelMapper.map(userReqDto, User.class);
        return user;
    }
    public UserResponse userEntityToUserRespDto(User user){
        UserResponse userResponse = this.modelMapper.map(user, UserResponse.class);
        return userResponse;
    }
}
