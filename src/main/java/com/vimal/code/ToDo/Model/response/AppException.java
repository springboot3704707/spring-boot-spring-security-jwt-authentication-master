package com.vimal.code.ToDo.Model.response;

import lombok.Data;
import org.springframework.http.HttpStatus;
@Data
public class AppException extends Exception{
    private HttpStatus httpStatus;
    private String errorCode;
    private String message;
    public AppException(){super();}
    public AppException(HttpStatus status, String message, Throwable cause){
        super(message, cause);
    }
    public AppException(HttpStatus status, String errorCode, String message){
        this.errorCode = errorCode;
        this.httpStatus = status;
        this.message = message;
    }

    @Override
    public String toString() {
        return "AppException{" +
                "httpStatus=" + httpStatus +
                ", errorCode='" + errorCode + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
