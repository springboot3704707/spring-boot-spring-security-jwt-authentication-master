package com.vimal.code.ToDo.Model;


import com.vimal.code.ToDo.Model.request.ItemKeyValue;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Entity
@Table(name = "categories")
@ToString
@Getter
@Setter

public class Categories {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String nameKh;
    private String status;
    @Transient
    private List<ItemKeyValue> statusList;

}
