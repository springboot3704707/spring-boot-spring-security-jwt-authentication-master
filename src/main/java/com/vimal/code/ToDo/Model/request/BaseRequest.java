package com.vimal.code.ToDo.Model.request;

import lombok.Data;

@Data
public class BaseRequest {
    private Integer limit;
    private Integer page;
    private String status;
    private String name;
    private Integer category_id;
    private Integer sub_category_id;
    private Integer id;
}
