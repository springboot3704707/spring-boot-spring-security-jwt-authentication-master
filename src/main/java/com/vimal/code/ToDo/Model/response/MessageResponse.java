package com.vimal.code.ToDo.Model.response;

public class MessageResponse
{
    private String code;
    private String message;
    private String messageKh;
    private String token;
    private Object data ;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public MessageResponse(String code, String message, String messageKh, String token, Object data) {
        this.code = code;
        this.message = message;
        this.messageKh = messageKh;
        this.token = token;
        this.data = data;
    }

    public MessageResponse() {
    }


    public  MessageResponse(String code, String message, String messageKh, Object data) {
        this.code = code;
        this.message = message;
        this.messageKh = messageKh;
        this.data = data;
    }
    public   MessageResponse(String code, String message,  Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
    public void getSuccess(Object data){
        this.code = "200";
        this.message = "Get Approval Success!";
        this.messageKh = "អនុម័តទិន្ន័យជោគជ័យ";
        this.data = data;
    }
    public void loginSuccess(Object data, String token){
        this.code = "200";
        this.message = "Login Success";
        this.messageKh = "ចូលក្នុងប្រព័ន្ធដោយជោគជ័យ";
        this.token = token;
        this.data = data;
    }
    public void updateSuccess(Object data){
        this.code = "200";
        this.message = "Updated Data Success!";
        this.messageKh = "កែប្រែទិន្ន័យជោគជ័យ";
        this.data = data;
    }
    public void createSuccess(Object data){
        this.code = "200";
        this.message = "Create Data Success!";
        this.messageKh = "បង្កើតទិន្ន័យជោគជ័យ";
        this.data = data;
    }


    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageKh() {
        return messageKh;
    }

    public Object getData() {
        return data;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMessageKh(String messageKh) {
        this.messageKh = messageKh;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
