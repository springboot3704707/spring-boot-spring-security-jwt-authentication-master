package com.vimal.code.ToDo.Model;

public enum Roles_Name {
    ROLE_ADMIN,
    ROLE_PM,
    ROLE_USER
}
