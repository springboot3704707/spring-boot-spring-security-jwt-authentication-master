package com.vimal.code.ToDo.repo;

import com.vimal.code.ToDo.Model.Categories;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepo extends JpaRepository<Categories, Integer> {
    List<Categories> findAllByStatus(String status);
    List<Categories> findAllByStatusInOrderByIdDesc(List<String> stringList);
    Optional<Categories> findByIdAndStatus(Integer id, String status);
    Optional<Categories> findById(Integer id);
}
