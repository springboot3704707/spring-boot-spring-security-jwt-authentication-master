package com.vimal.code.ToDo.repo;

import com.vimal.code.ToDo.Model.Role;
import com.vimal.code.ToDo.Model.Roles_Name;
import com.vimal.code.ToDo.constant.Constants;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
   Optional<Role> findByName(@NonNull Roles_Name name);

}
