package com.vimal.code.ToDo.repo;

import com.vimal.code.ToDo.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User,Long> {

    public Optional<User> findByEmail(String email);
    Optional<User> findByName(String name);


}
