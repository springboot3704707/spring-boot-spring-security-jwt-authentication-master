package com.vimal.code.ToDo.constant;

import com.vimal.code.ToDo.Model.request.ItemKeyValue;

import java.util.ArrayList;
import java.util.List;

public abstract class Constants {
    public static final String BAD_REQUEST = "Bad Request";
    public static final String STATUS_ACTIVE = "ACT";
    public static final String STATUS_DELETE = "DEL";
    public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";

    public enum Roles {
        ROLE_USER,
        ROLE_MODERATOR,
        ROLE_ADMIN
    }

    public static List<ItemKeyValue> getAllStatus(){
        List<ItemKeyValue> itemKeyValues = new ArrayList<>();
        itemKeyValues.add(new ItemKeyValue(1,"ACT","Active"));
        itemKeyValues.add(new ItemKeyValue(2,"DEL","Delete"));
        itemKeyValues.add(new ItemKeyValue(3,"DSL","Disable"));
        return itemKeyValues;
    }
    public static List<String> getAllStatusString(){
        List<String> stringList = new ArrayList<>();
        stringList.add("ACT");
        stringList.add("DEL");
        stringList.add("DSL");
        return stringList;
    }

}
